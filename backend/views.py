from http import HTTPStatus

from django.http import Http404, JsonResponse
from django.shortcuts import get_object_or_404
from django.db.models import Q
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from .logic.getters import get_clothes_list_for_user
from .serializers import ClothesSerializer, ProfileSerializer, CitySerializer
from .models import Clothes, Profile, City


class ClothesViewSet(viewsets.ModelViewSet):
    serializer_class = ClothesSerializer

    def get_queryset(self):
        name = self.request.GET.get('name')
        if name:
            clothes = Clothes.objects.filter(name__exact=name)
            if clothes.count() > 0:
                return clothes
            else:
                raise Http404
        else:
            return Clothes.objects.all()

    @action(detail=False, methods=['get'])
    def clothes_list_for_user(self, request):
        temp = self.request.GET.get('temp')
        gender = self.request.GET.get('gender')
        if not (temp and gender):
            return JsonResponse(
                {'error': 'Передано недостаточно аргументов. Требуется: temp, gender'},
                status=HTTPStatus.BAD_REQUEST
            )
        clothes = get_clothes_list_for_user(int(temp), gender)
        serializer = self.get_serializer(clothes, many=True)
        return Response(serializer.data)


class ProfileViewSet(viewsets.ModelViewSet):
    serializer_class = ProfileSerializer

    def get_queryset(self):
        telegram_user_id = self.request.GET.get('user_id')
        if telegram_user_id:
            account = Profile.objects.filter(telegram_user_id__exact=telegram_user_id)
            if account.count() > 0:
                return account
            else:
                raise Http404
        else:
            return Profile.objects.all()

    @action(detail=True, methods=['get'])
    def user_city(self, request, pk=None):
        user = get_object_or_404(Profile, pk=pk)
        serializer = self.get_serializer(user)
        return Response(serializer.data['city'])

    @action(detail=False, methods=['get'])
    def active_notification(self, request):
        user_list = Profile.objects.filter(notification_enabled__exact=True)
        page = self.paginate_queryset(user_list)

        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(user_list, many=True)
        return Response(serializer.data)

    @action(detail=False, methods=['get'])
    def statistics(self, request):
        user_list = Profile.objects.all()
        user_count = user_list.count()
        man_count = user_list.filter(gender__iexact='Мужской').count()
        woman_count = user_list.filter(gender__iexact='Женский').count()
        user_with_notif = user_list.filter(notification_enabled=1).count()
        admin_count = user_list.filter(is_admin=1).count()
        response_json = {
            'user_count': user_count,
            'man_count': man_count,
            'woman_count': woman_count,
            'user_with_notif': user_with_notif,
            'admin_count': admin_count
        }
        return Response(response_json)

    @action(detail=False, methods=['get'])
    def admin_list(self, request):
        admin_list = Profile.objects.filter(is_admin=1)
        serializer = self.get_serializer(admin_list, many=True)
        return Response(serializer.data)


class CityViewSet(viewsets.ModelViewSet):
    serializer_class = CitySerializer

    def get_queryset(self):
        name_ru = self.request.GET.get('name_ru')
        name_en = self.request.GET.get('name_en')
        if name_ru:
            city_ru = City.objects.filter(name_ru__iexact=name_ru)
            if city_ru.count() > 0:
                return city_ru
            else:
                raise Http404

        if name_en:
            city_en = City.objects.filter(name_en__iexact=name_en)
            if city_en.count() > 0:
                return city_en
            else:
                raise Http404

        return City.objects.all()
