from django.db import models


class City(models.Model):
    name_ru = models.CharField(
        verbose_name='Название (на русском)',
        max_length=150,
        blank=True,
    )
    name_en = models.CharField(
        verbose_name='Название (на английском)',
        max_length=150,
        blank=True,
    )
    country = models.CharField(
        verbose_name='Страна',
        max_length=150,
        blank=True, null=True,
    )

    def __str__(self):
        return self.name_ru

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'


class Clothes(models.Model):
    MALE = 'Мужской'
    FEMALE = 'Женский'
    UNISEX = 'Унисекс'
    GENDER_TYPES = [
        (MALE, 'Мужской'),
        (FEMALE, 'Женский'),
        (UNISEX, 'Унисекс'),
    ]
    HEADDRESS = '1'
    OUTERWEAR = '2'
    UPPER_TORSO = '3'
    UNDER_TORSO = '4'
    UPPER_PANTS = '5'
    UNDER_PANTS = '6'
    SHOES = '7'
    SOCKS = '8'
    ACCESSORY = '9'
    CLOTHES_TYPES = [
        (HEADDRESS, 'Головной убор'),
        (OUTERWEAR, 'Верхняя одежда'),
        (UPPER_TORSO, 'Торс (верхний)'),
        (UNDER_TORSO, 'Торс (нижний)'),
        (UPPER_PANTS, 'Штаны (верхний)'),
        (UNDER_PANTS, 'Штаны (нижний)'),
        (SHOES, 'Обувь'),
        (SOCKS, 'Носки'),
        (ACCESSORY, 'Аксессуары'),
    ]

    name = models.CharField(
        verbose_name='Название',
        max_length=150,
        blank=True,
    )
    gender = models.CharField(
        verbose_name='Пол',
        max_length=20,
        choices=GENDER_TYPES
    )
    temp_min = models.FloatField(verbose_name='Минимальная температура')
    temp_max = models.FloatField(verbose_name='Максимальная температура')
    type = models.CharField(
        verbose_name='Тип одежды',
        max_length=50,
        choices=CLOTHES_TYPES
    )
    temp_index = models.FloatField(verbose_name='Температурный индекс одежды', blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Одежда'
        verbose_name_plural = 'Одежды'


class Profile(models.Model):
    MALE = 'Мужской'
    FEMALE = 'Женский'
    GENDER_TYPES = [
        (MALE, 'Мужской'),
        (FEMALE, 'Женский'),
    ]
    telegram_user_id = models.IntegerField(
        verbose_name='ID пользователя в телеграмме',
        blank=True,
    )
    gender = models.CharField(
        verbose_name='Пол',
        max_length=20,
        choices=GENDER_TYPES,
        blank=True, null=True,
    )
    city = models.CharField(
        verbose_name='Город проживания',
        max_length=150,
        blank=True, null=True,
    )
    individual_temp_index = models.FloatField(
        verbose_name='Индивидуальный температурный индекс',
        blank=True, null=True,
        default=False,
    )
    timezone = models.CharField(
        verbose_name='Часовой пояс',
        max_length=100,
        blank=True, null=True,
    )
    notification_hour = models.PositiveIntegerField(
        verbose_name='Час уведомления',
        blank=True, null=True,
    )
    notification_minute = models.PositiveIntegerField(
        verbose_name='Минута уведомления',
        blank=True, null=True,
    )
    notification_enabled = models.BooleanField(
        verbose_name='Включены уведомления',
        blank=True, null=True,
        default=False,
    )
    is_admin = models.BooleanField(
        verbose_name='Является администратором',
        blank=True, null=True,
        default=False,
    )

    def __str__(self):
        return str(self.telegram_user_id)

    class Meta:
        verbose_name = 'Аккаунт'
        verbose_name_plural = 'Аккаунты'
