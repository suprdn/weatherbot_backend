from django.contrib import admin
from .models import Clothes, Profile, City

admin.site.register(Clothes)
admin.site.register(Profile)
admin.site.register(City)
