from . import models
from rest_framework import serializers


class ClothesSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Clothes
        fields = '__all__'


class ProfileSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Profile
        fields = '__all__'


class CitySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.City
        fields = '__all__'
