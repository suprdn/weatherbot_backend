from ..models import Clothes

BASE_HEAD_INDEX = 17
NECESSARY_CLOTHES_TYPES_MAP = {
    Clothes.HEADDRESS: False,
    Clothes.OUTERWEAR: False,
    Clothes.UPPER_TORSO: False,
    Clothes.UNDER_TORSO: True,
    Clothes.UPPER_PANTS: True,
    Clothes.UNDER_PANTS: False,
    Clothes.SHOES: True,
    Clothes.SOCKS: False,
    Clothes.ACCESSORY: False,
}