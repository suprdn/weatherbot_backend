from rest_framework import routers
from .views import ClothesViewSet, ProfileViewSet, CityViewSet

router = routers.DefaultRouter()
router.register(r'clothes', ClothesViewSet, basename='clothes')
router.register(r'profile', ProfileViewSet, basename='profile')
router.register(r'city', CityViewSet, basename='city')

urlpatterns = router.urls
