import itertools
from typing import Optional
from django.db.models import Q

from ..consts.const import BASE_HEAD_INDEX
from ..models import Clothes


def nearest_value(items, value):
    found = items[0]
    for item in items:
        if item is None:
            continue
        if abs(item - value) < abs(found - value):
            found = item
    return found


def clothe_with_nearest_temp_index(clothes, temp_index):
    found = clothes[0]
    for item in clothes:
        if abs(item.temp_index - temp_index) < abs(found.temp_index - temp_index):
            found = item
    return found


def torso_clothes_list(outerwear, upper_torso, under_torso, temp_index):
    outerwear_upper_under = list(itertools.product(outerwear, upper_torso, under_torso))
    outerwear_upper = list(itertools.product(outerwear, upper_torso))
    outerwear_under = list(itertools.product(outerwear, under_torso))
    upper_under = list(itertools.product(upper_torso, under_torso))
    all_clothes_choices = outerwear_upper_under + outerwear_upper + outerwear_under + upper_under + list(under_torso) \
                          + list(upper_torso)
    all_clothes_choices_temp_index = []
    for clothes_couples in all_clothes_choices:
        all_temp_index = 0
        if isinstance(clothes_couples, tuple):
            for clothe in clothes_couples:
                if clothe.temp_index is None:
                    continue
                all_temp_index += clothe.temp_index
        else:
            all_temp_index = clothes_couples.temp_index
        all_clothes_choices_temp_index.append(all_temp_index)
    optimal_clothes_temp_index = nearest_value(all_clothes_choices_temp_index, temp_index)
    index = all_clothes_choices_temp_index.index(optimal_clothes_temp_index)
    clothes = all_clothes_choices[index]
    return clothes


def pants_clothes_list(upper_pants, under_pants, temp_index):
    upper_under = list(itertools.product(upper_pants, under_pants))
    excluded_couple = (Clothes.objects.filter(name="Шорты").first(), Clothes.objects.filter(name='Термобелье').first())
    upper_under.remove(excluded_couple)
    all_clothes_choices = upper_under + list(upper_pants)
    all_temp_index = []
    for clothes_couples in all_clothes_choices:
        sum_temp_index = 0
        if isinstance(clothes_couples, tuple):
            for clothe in clothes_couples:
                if clothe.temp_index is None:
                    continue
                sum_temp_index += clothe.temp_index
        else:
            sum_temp_index = clothes_couples.temp_index
        all_temp_index.append(sum_temp_index)
    optimal_temp_index = nearest_value(all_temp_index, temp_index)
    index = all_temp_index.index(optimal_temp_index)
    clothes = all_clothes_choices[index]
    if not isinstance(clothes, (list, tuple)):
        clothes = [clothes]
    return clothes


def get_clothes_list_for_user(temp: Optional[int], gender: Optional[str]):
    temp_index = -(2 / 3) * temp + 22
    clothes_list_gendered = Clothes.objects.filter(Q(gender=gender) | Q(gender=Clothes.UNISEX))
    headdress = clothes_list_gendered.filter(type=Clothes.HEADDRESS)
    outerwear = clothes_list_gendered.filter(type=Clothes.OUTERWEAR)
    upper_torso = clothes_list_gendered.filter(type=Clothes.UPPER_TORSO)
    under_torso = clothes_list_gendered.filter(type=Clothes.UNDER_TORSO)
    upper_pants = clothes_list_gendered.filter(type=Clothes.UPPER_PANTS)
    under_pants = clothes_list_gendered.filter(type=Clothes.UNDER_PANTS)
    shoes = clothes_list_gendered.filter(type=Clothes.SHOES)

    headdress_need = clothe_with_nearest_temp_index(headdress, temp_index)
    torso_need = torso_clothes_list(outerwear, upper_torso, under_torso, temp_index)
    pants_need = pants_clothes_list(upper_pants, under_pants, temp_index)
    shoes_need = clothe_with_nearest_temp_index(shoes, temp_index)

    clothes_id_list = []
    if hasattr(headdress_need, 'id') and abs(temp_index - (int(headdress_need.temp_index) + BASE_HEAD_INDEX)) < 10:
        clothes_id_list.append(headdress_need.id)
    for clothe in torso_need:
        clothes_id_list.append(clothe.id)
    for clothe in pants_need:
        clothes_id_list.append(clothe.id)
    if hasattr(shoes_need, 'id'):
        clothes_id_list.append(shoes_need.id)

    clothes = Clothes.objects.filter(id__in=clothes_id_list)
    return clothes
